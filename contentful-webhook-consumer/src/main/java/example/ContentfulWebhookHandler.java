package example;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.function.aws.MicronautRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Introspected
public class ContentfulWebhookHandler extends MicronautRequestHandler<Object, Object> {

  Logger log = LoggerFactory.getLogger(ContentfulWebhookHandler.class);

  @Override
  public Object execute(Object input) {
    log.info("Received event: {}", input);
    return input;
  }
}
