package example;

import com.contentful.java.cda.TransformQuery.ContentfulEntryModel;
import com.contentful.java.cda.TransformQuery.ContentfulField;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ContentfulEntryModel("ruleV1")
public class Rule {

  @ContentfulField private String id;

  @ContentfulField private String ruleName;

  @ContentfulField private String merchant;

  @ContentfulField private String timeUnit;

  // double due to https://github.com/contentful/contentful.java/issues/219
  @ContentfulField private Double spanOfTimeUnits;

  @ContentfulField private Double txValueMax;

  @ContentfulField private Double txValueMin;

  @ContentfulField private Double txCountMax;

  @ContentfulField private Double txCountMin;
}
