package example;

import com.contentful.java.cda.CDAClient;
import com.contentful.java.cda.CDAEntry;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.function.aws.MicronautRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

@Introspected
public class RuleExampleHandler extends MicronautRequestHandler<Object, List<Rule>> {

  Logger log = LoggerFactory.getLogger(RuleExampleHandler.class);

  @Override
  public List<Rule> execute(Object input) {
    // https://github.com/contentful/contentful.java
    CDAClient client =
        CDAClient.builder()
            .setSpace("ijkshe236jiw")
            .setToken("-eMtKZXOcplMlH463vaGRXZta9z7oFECrasHX9ZBLl0")
            .build();

    var array = client.fetch(CDAEntry.class).withContentType("ruleV1").all();

    log.info("CDA Entry, ruleV1 {}", array.items());

    var rules = client.observeAndTransform(Rule.class).all().toList().blockingGet();

    log.info("Rules {}", rules.get(0));

    return rules.get(0).stream().collect(Collectors.toList());
  }
}
