
To build and run:

```bash
sam build 

sam local invoke RuleExample -t ./.aws-sam/build/template.yaml
```

---

Contains postman collection to consume the contentful rules, demo the API

`Contentful Rule API.postman_collection.json`

---

Contentful documentation:

<https://www.contentful.com/developers/docs/references/content-delivery-api/>

---

example of webhook event emitted by contentful in:

`example-webhook-event.json`

update reads:

```json
        "revision": 2,
        "createdAt": "2021-07-01T14:48:41.621Z",
        "updatedAt": "2021-07-01T15:03:59.181Z"
```

create states `revision: 1` where `createdAt` and `updatedAt` are equal values
